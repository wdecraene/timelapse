#!/bin/bash

for d in *; do
  if [[ -d $d ]]; then
    pushd $d > /dev/null

    for f in * ; do D=$(stat -nf'%B' "$f") ; DF=$(date -r $D '+%Y%m%d_%H%M'); echo $DF; mv "$f" "../$DF.JPG" ; done

    counter=$(ls -1 | wc -l)

    popd > /dev/null

    if [ "$counter" -eq "0" ]; then
      rm -r $d
    fi
  fi
done
