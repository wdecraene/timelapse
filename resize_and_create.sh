#!/bin/bash

# Global functions and variables
declare -a months
months[1]=jan
months[2]=feb
months[3]=maa
months[4]=apr
months[5]=mei
months[6]=jun
months[7]=jul
months[8]=aug
months[9]=sep
months[10]=okt
months[11]=nov
months[12]=dec

function toTimestamp {
    date -j -u -f "%Y%m%d_%H%M" $1 "+%s"
}

function getPosition {
    unit=$( echo "($WIDTH - ( 2 * $SIDE_MARGIN) ) / ($TIMESTAMP_END - $TIMESTAMP_START)" | bc -l )

    position=$( echo "$SIDE_MARGIN + ($unit * ($1 - $TIMESTAMP_START))" | bc -l )

    result=$( echo "($position+0.5)/1" | bc )
    echo $result
}

# Variables
START=20170501_0000 # first day of month
END=20190131_2359 # last day of month

ROTATION=-3.2

ZOOM_WIDTH=2512
ZOOM_HEIGHT=1413
ZOOM_X=50
ZOOM_Y=250

WIDTH=1920
HEIGHT=1080

SIDE_MARGIN=50
BOTTOM_MARGIN=80
THICKNESS=4

RADIUS_SMALL=8
RADIUS_LARGE=16

# Calculations
TIMESTAMP_START=$(toTimestamp $START)
TIMESTAMP_END=$(toTimestamp $END)

CENTER=$(expr $HEIGHT - $BOTTOM_MARGIN)

# Make new directory
mkdir -p RESIZED_PHOTO_DIRECTORY
mkdir -p ORIGINAL_PHOTO_DIRECTORY

# COUNTER
COUNT=0
TOTAL=`find . -maxdepth 1 -type f -name '*.JPG' | wc -l`
echo $TOTAL
PSTR="[=======================================================================]"

# Generate images
# for IMAGE in `ls *.JPG`; do
for IMAGE in *.JPG; do

  # Display progress
  COUNT=$(( $COUNT + 1 ))
  PD=$(( $COUNT * 73 / $TOTAL ))
  printf "\r$IMAGE $COUNT %3d.%1d%% %.${PD}s" $(( $COUNT * 100 / $TOTAL )) $(( ($COUNT * 1000 / $TOTAL) % 10 )) $PSTR

  if [ ! -f "RESIZED_PHOTO_DIRECTORY/$IMAGE" ]
  then

    TIMESTAMP_IMAGE=$(toTimestamp ${IMAGE%.*})
    POSITION_IMAGE=$(getPosition $TIMESTAMP_IMAGE)

    # Init command
    COMMAND="convert $IMAGE -quality 100"

    # Better colors
    COMMAND+=" -auto-level -auto-gamma"

    # Make building horizontal
    COMMAND+=" -rotate ${ROTATION}"

    # Remove fisheye
    COMMAND+=" -distort barrel '0 0 -0.3'"

    # Resize and crop image
    COMMAND+=" -gravity south -crop ${ZOOM_WIDTH}x${ZOOM_HEIGHT}+${ZOOM_X}+${ZOOM_Y} +repage"
    COMMAND+=" -resize ${WIDTH}x${HEIGHT}^"

    # Set color
    COMMAND+=" -fill white"

    # Draw horizontal line
    COMMAND+=" -draw 'rectangle ${SIDE_MARGIN},`expr $HEIGHT - $BOTTOM_MARGIN` `expr $WIDTH - $SIDE_MARGIN`,`expr $HEIGHT - $BOTTOM_MARGIN + $THICKNESS`'"

    # Draw dots, months, year
    YEAR=0
    TIMESTAMP=$TIMESTAMP_START
    while [ $TIMESTAMP -le $TIMESTAMP_END ]; do
        ACTIVE_MONTH=$(date -r $TIMESTAMP '+%-m')
        ACTIVE_YEAR=$(date -r $TIMESTAMP '+%Y')

        POSITION=$(getPosition $TIMESTAMP)

        if [ $YEAR -ne $ACTIVE_YEAR ]; then
            COMMAND+=" -pointsize 32 -font Ubuntu-Bold"
            COMMAND+=" -gravity SouthWest -draw 'text `expr $POSITION - 35`,`expr $BOTTOM_MARGIN + $RADIUS_LARGE` \"$ACTIVE_YEAR\"'"

            COMMAND+=" -draw 'circle ${POSITION},${CENTER} `expr $POSITION + $RADIUS_LARGE`,${CENTER}'"
        else
            COMMAND+=" -draw 'circle ${POSITION},${CENTER} `expr $POSITION + $RADIUS_SMALL`,${CENTER}'"
        fi

        COMMAND+=" -pointsize 20 -font Ubuntu-Condensed"
        COMMAND+=" -gravity SouthWest -draw 'text `expr $POSITION - 12`,`expr $BOTTOM_MARGIN - 40` \"${months[$ACTIVE_MONTH]}\"'"

        let YEAR=$ACTIVE_YEAR
        let TIMESTAMP=$(date -r $TIMESTAMP -v+1m +'%s')
    done

    # Add position
    COMMAND+=" -fill red -draw 'circle ${POSITION_IMAGE},${CENTER} `expr $POSITION_IMAGE + $RADIUS_SMALL`,${CENTER}'"

    # Set output
    COMMAND+=" RESIZED_PHOTO_DIRECTORY/$IMAGE"

    # Execute
    eval $COMMAND

  fi

  mv $IMAGE ORIGINAL_PHOTO_DIRECTORY/$IMAGE
done

# Go to resized images
cd RESIZED_PHOTO_DIRECTORY

# Create video
ffmpeg -r 24 -pattern_type glob -i '*.JPG' -s hd1080 -vcodec libx264 -crf 18 -preset slow timelapse.mp4

