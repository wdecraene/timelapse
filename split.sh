#!/bin/bash

mkdir -p odd
mkdir -p even

COUNT=0

for IMAGE in *.JPG; do
  COUNT=$(( $COUNT + 1 ))

  echo $IMAGE

  if (( $COUNT % 2 )); then
    mv $IMAGE odd/$IMAGE
  else
    mv $IMAGE even/$IMAGE
  fi
done
