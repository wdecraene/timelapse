# Timelapse scripts

Pictures taken with GoPro Hero4 and Cam-Do Blink (https://cam-do.com/collections/all/products/blink-gopro-time-lapse-controller).

## rename.sh

Loop through all subdirectories, rename all jpg files with a date in their name (%Y%m%d_%H%M) and move them one directory up.

## split.sh

Simple script to split a list of JPG files in two, this is used when too many images are taken for the timelapse.

## resize_and_create.sh

Loop through all JPG files, transform them and create a timelapse with them.
Transforming: remove fisheye, rotate, scale, add timeline.
